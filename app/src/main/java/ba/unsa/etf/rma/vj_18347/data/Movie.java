package ba.unsa.etf.rma.vj_18347.data;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Movie implements Parcelable {

    private String title;
    private String overview;
    private String releaseDate;
    private String homepage;

    public ArrayList<String> getSimilarMovies() {
        return similarMovies;
    }

    public void setSimilarMovies(ArrayList<String> similarMovies) {
        this.similarMovies = similarMovies;
    }

    private String genre;
    private ArrayList<String> actors;
    private ArrayList<String> similarMovies;

    protected Movie(Parcel in) {
        title = in.readString();
        overview = in.readString();
        releaseDate = in.readString();
        homepage = in.readString();
        genre = in.readString();
        actors = in.createStringArrayList();
        similarMovies = in.createStringArrayList();
    }

    public static final Creator<Movie> CREATOR = new Creator<Movie>() {
        @Override
        public Movie createFromParcel(Parcel in) {
            return new Movie(in);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };

    public ArrayList<String> getActors() {
        return actors;
    }

    public void setActors(ArrayList<String> actors) {
        this.actors = actors;
    }

    public Movie(String title, String overview, String releaseDate, String homepage, String genre, ArrayList<String> actors, ArrayList<String> similarMovies) {
        this.title = title;
        this.overview = overview;
        this.releaseDate = releaseDate;
        this.homepage = homepage;
        this.genre = genre;
        this.actors=actors;
        this.similarMovies = similarMovies;
    }
    public Movie(String title, String overview, String releaseDate, String homepage, String genre, ArrayList<String> actors) {
        this.title = title;
        this.overview = overview;
        this.releaseDate = releaseDate;
        this.homepage = homepage;
        this.genre = genre;
        this.actors=actors;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getHomepage() {
        return homepage;
    }

    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(overview);
        dest.writeString(genre);
        dest.writeString(releaseDate);
        dest.writeString(homepage);
        dest.writeStringList(actors);
        dest.writeStringList(similarMovies);
    }
}
