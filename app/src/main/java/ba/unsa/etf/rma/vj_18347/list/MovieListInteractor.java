package ba.unsa.etf.rma.vj_18347.list;
import java.util.ArrayList;

import ba.unsa.etf.rma.vj_18347.data.Movie;
import ba.unsa.etf.rma.vj_18347.data.MoviesModel;

public class MovieListInteractor implements IMovieListInteractor {

    @Override
    public ArrayList<Movie> get() {
        return MoviesModel.movies;
    }
}
