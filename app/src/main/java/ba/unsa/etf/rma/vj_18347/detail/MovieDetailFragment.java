package ba.unsa.etf.rma.vj_18347.detail;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.fragment.app.Fragment;

import java.lang.reflect.Field;

import ba.unsa.etf.rma.vj_18347.R;
import ba.unsa.etf.rma.vj_18347.data.Movie;

public class MovieDetailFragment extends Fragment {

    private TextView title;
    private TextView genre;
    private TextView overview;
    private TextView releaseDate;
    private TextView homepage;
    private ListView actors;
    private Button share;
    private ToggleButton toggleButton;
    private ImageView imageView;

    private IMovieDetailPresenter presenter;

    public IMovieDetailPresenter getPresenter() {
        if (presenter == null) {
            presenter = new MovieDetailPresenter(getActivity());
        }
        return presenter;
    }
    private View.OnClickListener homepageOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String url = getPresenter().getMovie().getHomepage();
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(url));
            if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                startActivity(intent);
            }
        }
    };

    private View.OnClickListener titleOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(Intent.ACTION_SEARCH);
            intent.setPackage("com.google.android.youtube");
            intent.putExtra("query", getPresenter().getMovie().getTitle() + " trailer");
            if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                startActivity(intent);
            }
        }
    };

    private View.OnClickListener shareOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_SEND);
            intent.putExtra(Intent.EXTRA_TEXT, getPresenter().getMovie().getOverview());
            intent.setType("text/plain");
            if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                startActivity(intent);
            }
        }
    };

    private CompoundButton.OnCheckedChangeListener toggleOnCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
                Bundle arguments = new Bundle();
                arguments.putStringArrayList("similar", getPresenter().getMovie().getSimilarMovies());
                SimilarFragment similarFragment = new SimilarFragment();
                similarFragment.setArguments(arguments);
                getChildFragmentManager().beginTransaction()
                        .replace(R.id.frame, similarFragment).commit();
            } else {
                Bundle arguments = new Bundle();
                arguments.putStringArrayList("cast", getPresenter().getMovie().getActors());
                CastFragment castFragment = new CastFragment();
                castFragment.setArguments(arguments);
                getChildFragmentManager().beginTransaction()
                        .replace(R.id.frame, castFragment).commit();
            }
        }
    };
    @Override
    public View onCreateView(
            LayoutInflater inflater,
            ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail, container, false);
        if (getArguments() != null && getArguments().containsKey("movie")) {
            getPresenter().setMovie(getArguments().getParcelable("movie"));
            title = view.findViewById(R.id.title);
            genre = view.findViewById(R.id.genre);
            overview = view.findViewById(R.id.overview);
            releaseDate = view.findViewById(R.id.releaseDate);
            homepage = view.findViewById(R.id.homepage);
            share = view.findViewById(R.id.share);
            toggleButton = view.findViewById(R.id.toggle_button);
            imageView = view.findViewById(R.id.icon);
            Movie movie = getPresenter().getMovie();
            title.setText(movie.getTitle());
            genre.setText(movie.getGenre());
            overview.setText(movie.getOverview());
            releaseDate.setText(movie.getReleaseDate());
            homepage.setText(movie.getHomepage());

            String genreMatch = movie.getGenre();
            try {
                Class res = R.drawable.class;
                Field field = res.getField(genreMatch);
                int drawableId = field.getInt(null);
                imageView.setImageResource(drawableId);
            } catch (Exception e) {
                imageView.setImageResource(R.drawable.picture1);
            }
            Bundle arguments = new Bundle();
            arguments.putStringArrayList("cast", movie.getActors());
            CastFragment castFragment = new CastFragment();
            castFragment.setArguments(arguments);
            getChildFragmentManager().beginTransaction()
                    .add(R.id.frame, castFragment)
                    .commit();

            homepage.setOnClickListener(homepageOnClickListener);
            title.setOnClickListener(titleOnClickListener);
            share.setOnClickListener(shareOnClickListener);
            toggleButton.setOnCheckedChangeListener(toggleOnCheckedChangeListener);
        }
        return  view;
    }

}
