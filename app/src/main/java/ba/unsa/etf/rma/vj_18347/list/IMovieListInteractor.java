package ba.unsa.etf.rma.vj_18347.list;

import java.util.ArrayList;

import ba.unsa.etf.rma.vj_18347.data.Movie;

public interface IMovieListInteractor {

    ArrayList<Movie> get();
}
