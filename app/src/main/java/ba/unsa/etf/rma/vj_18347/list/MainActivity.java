package ba.unsa.etf.rma.vj_18347.list;

import android.content.IntentFilter;
import android.os.Bundle;
import android.widget.FrameLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import ba.unsa.etf.rma.vj_18347.R;
import ba.unsa.etf.rma.vj_18347.data.Movie;
import ba.unsa.etf.rma.vj_18347.detail.MovieDetailFragment;
import ba.unsa.etf.rma.vj_18347.util.ConnectivityBroadcastReceiver;

public class MainActivity extends AppCompatActivity implements MovieListFragment.OnItemClick{

    private boolean twoPaneMode;
    private ConnectivityBroadcastReceiver receiver = new ConnectivityBroadcastReceiver();
    private IntentFilter filter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FrameLayout details = findViewById(R.id.movie_detail);
        if (details != null) {
            twoPaneMode = true;
            MovieDetailFragment detailFragment = (MovieDetailFragment) fragmentManager.findFragmentById(R.id.movie_detail);
            if (detailFragment==null) {
                detailFragment = new MovieDetailFragment();
                fragmentManager.beginTransaction().
                        replace(R.id.movie_detail,detailFragment)
                        .commit();
            }
        } else {

            twoPaneMode = false;
        }
        Fragment listFragment =  fragmentManager.findFragmentById(R.id.movies_list);
        if (listFragment==null){
            listFragment = new MovieListFragment();
            fragmentManager.beginTransaction()
                    .replace(R.id.movies_list,listFragment)
                    .commit();
        }
        else{
            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    @Override
    public void onResume() {

        super.onResume();
        registerReceiver(receiver, filter);
    }

    @Override
    public void onPause() {

        unregisterReceiver(receiver);
        super.onPause();
    }

    @Override
    public void onItemClicked(Movie movie) {
        Bundle arguments = new Bundle();
        arguments.putParcelable("movie", movie);
        MovieDetailFragment detailFragment = new MovieDetailFragment();
        detailFragment.setArguments(arguments);
        if (twoPaneMode){
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.movie_detail, detailFragment)
                    .commit();
        }
        else{
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.movies_list,detailFragment)
                    .addToBackStack(null)
                    .commit();
        }
    }

}
