package ba.unsa.etf.rma.vj_18347.detail;

import android.os.Parcelable;

import java.util.ArrayList;

import ba.unsa.etf.rma.vj_18347.data.Movie;

public interface IMovieDetailPresenter {

    void create(String title, String overview, String releaseDate, String genre, String homepage, ArrayList<String> actors);
    void setMovie(Parcelable movie);
    Movie getMovie();
}
