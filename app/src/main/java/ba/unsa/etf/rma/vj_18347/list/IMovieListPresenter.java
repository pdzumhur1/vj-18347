package ba.unsa.etf.rma.vj_18347.list;

public interface IMovieListPresenter {

    void refreshMovies();
}
